using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;

namespace Restoran.Models
{
    public class Client : Entity //Клиент
    {
        public string Name { get; set; }
        public string Contact { get; set; }
    }
}