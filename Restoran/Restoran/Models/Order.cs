using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using Microsoft.AspNetCore.Mvc;

namespace Restoran.Models
{
    public class Order : Entity
    {
        [FromForm(Name = "name")] public string Name { get; set; }
        [FromForm(Name = "dishOrders")] public string DishOrder { get; set; }

        public static DateTime? DateOrders { get; set; }

        [FromForm(Name = "institutionId")]
        [ForeignKey("Institution")]
        public int InstitutionId { get; set; }

        public Institution Institution { get; set; }

        [FromForm(Name = "dishId")]
        [ForeignKey("Dish")]
        public int DishId { get; set; }

        public Dish Dish { get; set; }
    }
}